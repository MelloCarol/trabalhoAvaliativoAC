//
//  ViewController.swift
//  trabalhoAvaliativoAC
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Dados: Codable{
    
    let image: String
    let name: String
    let actor: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeDados:[Dados] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeDados.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Celula", for: indexPath) as! MyCell
        
        let dadosPessoa = listaDeDados[indexPath.row]
        
        cell.nomePersonagem.text = dadosPessoa.name
        cell.nomeAtor.text = dadosPessoa.actor
        cell.imagem.kf.setImage(with: URL(string: dadosPessoa.image))
        return cell
        
    }
    

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Dados].self){(response) in
            self.view.isUserInteractionEnabled = false
            if let dados = response.value{
                self.listaDeDados = dados
                self.view.isUserInteractionEnabled = true
            }
            self.tableview.reloadData()
        }
    }


}

