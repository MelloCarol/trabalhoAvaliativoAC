//
//  MyCell.swift
//  trabalhoAvaliativoAC
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var nomePersonagem: UILabel!
    @IBOutlet weak var nomeAtor: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
